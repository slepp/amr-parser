FROM node:6

COPY ./package.json /app/

WORKDIR /app

RUN npm install

COPY ./index.js /app/

CMD [ "npm", "run", "start" ]
