AMR Log File Parser
===================

This is a simple program to watch the log files coming out of RTLAMR in JSON format
and convert them into messages for MQTT and StatsD.

It is designed to be used inside of Docker, where the log files are in `/logs/` and named
`scm.log` and `scmplus.log`. The settings for MQTT and StatsD hosts are:

* `MQTT_HOST`, `MQTT_USERNAME`, `MQTT_PASSWORD`: MQTT settings
* `STATSD_HOST`, `STATSD_PORT`: StatsD host and port
