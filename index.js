var Tail = require('tail').Tail;
var mqtt = require('mqtt');
var SDC = require("statsd-client");

var stats = new SDC({
  host: process.env.STATSD_HOST || "localhost",
  port: process.env.STATSD_PORT || 8125,
  tcp: false,
  debug: true
});

var client = mqtt.connect({
  host: process.env.MQTT_HOST || "localhost",
  username: process.env.MQTT_USERNAME,
  password: process.env.MQTT_PASSWORD
});

tailSCM = new Tail("/logs/scm.log");
tailSCMPlus = new Tail("/logs/scmplus.log");

client.on('connect', function() {
	console.log("Connected to MQTT");
});

var history = {scm:{}, scmplus:{}};

tailSCM.on("line", function(data) {
	try {
		var d = JSON.parse(data);
		var type = 'generic';
		switch(d.Message.Type) {
                        case 0:
                        case 1:
                        case 2:
                        case 9:
			case 12:
			type = 'gas'; break;

		        case 3:
		        case 11:
		        case 13:
			type = 'water'; break;

			case 4:
			case 5:
			case 7:
			case 8:
			type = 'electric'; break;
		}
		if(!(history.scm[d.Message.ID] instanceof Array)) {
			history.scm[d.Message.ID] = new Array;
		}
		if(history.scm[d.Message.ID].length != 0) {
			if(history.scm[d.Message.ID][0].consumption != d.Message.Consumption) {
				history.scm[d.Message.ID].push({ts:new Date(), consumption: d.Message.Consumption});
			}
		} else {
			history.scm[d.Message.ID].push({ts:new Date(), consumption: d.Message.Consumption});
		}
		client.publish("amr/"+type+"/"+d.Message.ID+"/current", ""+d.Message.Consumption);
		stats.gauge('environmental.'+type+'.'+d.Message.ID+'.current', d.Message.Consumption);
		var now = new Date().getTime();
		history.scm[d.Message.ID] = history.scm[d.Message.ID].filter(function(i) {
			return (now - i.ts.getTime()) < (24*60*60*1000);
		});
		if(history.scm[d.Message.ID].length > 0) {
			client.publish("amr/"+type+"/"+d.Message.ID+"/24hour", ""+(d.Message.Consumption-history.scm[d.Message.ID][0].consumption));
			stats.gauge('environmental.'+type+'.'+d.Message.ID+'.24hour', (d.Message.Consumption-history.scm[d.Message.ID][0].consumption));
		}
		now = new Date();
		now.setHours(0);
		now.setMinutes(0);
		now.setSeconds(0);
		now.setMilliseconds(0);
		now = now.getTime();
		var tmp = history.scm[d.Message.ID].filter(function(i) {
			return i.ts.getTime() > now;
		});
		if(tmp.length > 0) {
			client.publish("amr/"+type+"/"+d.Message.ID+"/today", ""+(d.Message.Consumption-tmp[0].consumption));
			stats.gauge('environmental.'+type+'.'+d.Message.ID+'.today', (d.Message.Consumption-tmp[0].consumption));
		}
	} catch(e) {
		console.log(e);
	}
});

tailSCMPlus.on("line", function(data) {
	try {
		var d = JSON.parse(data);
		var type = 'generic';
		switch(d.Message.EndpointType) {
			case 171: type = 'water'; d.Message.Consumption = d.Message.Consumption/10.0; break;
		}
		if(!(history.scmplus[d.Message.EndpointID] instanceof Array)) {
			history.scmplus[d.Message.EndpointID] = new Array;
		}
		if(history.scmplus[d.Message.EndpointID].length != 0) {
			if(history.scmplus[d.Message.EndpointID][0].consumption != d.Message.Consumption) {
				history.scmplus[d.Message.EndpointID].push({ts:new Date(), consumption: d.Message.Consumption});
			}
		} else {
			history.scmplus[d.Message.EndpointID].push({ts:new Date(), consumption: d.Message.Consumption});
		}
		client.publish("amr/"+type+"/"+d.Message.EndpointID+"/current", ""+d.Message.Consumption);
		stats.gauge('environmental.'+type+'.'+d.Message.EndpointID+'.current', d.Message.Consumption);
		var now = new Date().getTime();
		history.scmplus[d.Message.EndpointID] = history.scmplus[d.Message.EndpointID].filter(function(i) {
			return (now - i.ts.getTime()) < (24*60*60*1000);
		});
		if(history.scmplus[d.Message.EndpointID].length > 0) {
			client.publish("amr/"+type+"/"+d.Message.EndpointID+"/24hour", ""+(d.Message.Consumption-history.scmplus[d.Message.EndpointID][0].consumption));
			stats.gauge('environmental.'+type+'.'+d.Message.EndpointID+'.24hour', (d.Message.Consumption-history.scmplus[d.Message.EndpointID][0].consumption));
		}
		now = new Date();
		now.setHours(0);
		now.setMinutes(0);
		now.setSeconds(0);
		now.setMilliseconds(0);
		now = now.getTime();
		var tmp = history.scmplus[d.Message.EndpointID].filter(function(i) {
			return i.ts.getTime() > now;
		});
		if(tmp.length > 0) {
			client.publish("amr/"+type+"/"+d.Message.EndpointID+"/today", ""+(d.Message.Consumption-tmp[0].consumption));
			stats.gauge('environmental.'+type+'.'+d.Message.EndpointID+'.today', (d.Message.Consumption-tmp[0].consumption));
		}
	} catch(e) {
		console.log(e);
	}
});

